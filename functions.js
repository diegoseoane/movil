const { moviles } = require("./objects.js");
const objects = require("./objects.js");

function menorPrecio(req, res) {
    let value = Infinity;
    let string = "";
    for (movil of objects.moviles){       
        if (movil.precio < value) {
            value = movil.precio;
            string = `El movil mas economico es ${movil.marca} ${movil.modelo}  (Valor: $${movil.precio})`
        }
    }
    res.send(string);
}

function mayorPrecio(req, res) {
    let value = Number.NEGATIVE_INFINITY;
    let string = "";
    for (movil of objects.moviles){       
        if (movil.precio > value) {
            value = movil.precio;
            string = `El movil mas caro es ${movil.marca} ${movil.modelo}  (Valor: $${movil.precio})`
        }
    }
    res.send(string);
}

function gama(req, res) {
    let gamaAlta = [];
    let gamaMedia = [];
    let gamaBaja = [];
    for (movil of objects.moviles) {
        if (movil.gama === "Alta") {
            gamaAlta.push(movil);
        } else if (movil.gama === "Media") {
            gamaMedia.push(movil);
        } else if (movil.gama === "Baja") {
            gamaBaja.push(movil);
        }
    }
    
    let string = "";
    string += "Los moviles gama baja son: <br>";
    for (cel of gamaBaja){
        string += cel.marca + " " + cel.modelo + "<br>";
    }
    string += "Los moviles gama media son: <br>";
    for (cel of gamaMedia){
        string += cel.marca + " " + cel.modelo + "<br>";
    }
    string += "Los moviles gama alta son: <br>";
    for (cel of gamaAlta){
        string += cel.marca + " " + cel.modelo + "<br>";
    }
    res.send(string)    
}
    
module.exports = {
    menorPrecio,
    mayorPrecio,
    gama
}
