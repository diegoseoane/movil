class Movil {
    constructor(marca, gama, modelo, pantalla, os, precio) {
        this.marca = marca;
        this.gama = gama;
        this.modelo = modelo;
        this.pantalla = pantalla;
        this.os = os;
        this.precio = precio; 
    }
}

let movil1 = new Movil("Samsung", "Media", "J4", "Mediana", "Android", 10000);
let movil2 = new Movil("Samsung", "Media", "J1", "Mediana", "Android", 12000);
let movil3 = new Movil("Alkatel", "Baja", "AA03", "Chica", "Windows", 8000);
let movil4 = new Movil("Alkatel", "Baja", "AA01", "Chica", "Windows", 7000);
let movil5 = new Movil("Iphone", "Alta", "3", "Grande", "Apple", 20000);
let moviles = [];
moviles.push(movil1, movil2, movil3, movil4, movil5);

module.exports = {
    moviles: moviles,
}

