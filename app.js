const express = require("express");
const objects = require("./objects.js");
const { menorPrecio, mayorPrecio, gama } = require("./functions.js");

const server = express();

server.get("/moviles", function (req, res) {
    res.send(objects.moviles);
})

server.get("/menorprecio", menorPrecio);
server.get("/mayorprecio", mayorPrecio);
server.get("/gama", gama);

server.listen(3000, function () {
    console.log("Server working");
})

